module.exports = {
	locales: ['en', 'ar'],
	defaultLocale: 'en',
	pages: {
		'*': ['common'],
		'/': ['common'],
		'/move/view_details': ['common'],
	},
	interpolation: {
		prefix: '${',
		suffix: '}',
	},
	loadLocaleFrom: (locale, namespace) => import(`./src/translations/${namespace}/${locale}`).then((m) => m.default),
};
