import React from "react";
import { FormItemProps, ModalProps } from "antd";
import { primaryColor, secondaryColor, smokeColor, thirdColor } from "./color";

import { Rule } from "antd/lib/form";

export const smallShadow = "#00000029 0px 3px 5px";
export const largeShadow = "#0004 0px 0px 20px";
export const cardShadow = "#00000029 0px 3px 10px";

export * from "./color";

export const responsive_constant = { xl: 20, md: 22, xs: 24 };

export const input_layout = {
  bordered: false,
  style: {
    width: "100%",
    minHeight: 50,
    borderRadius: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontFamily: "Source Sans Pro",
    backgroundColor: "none",
    border: "1px solid #F6CF34",
    color: primaryColor,
  } as React.CSSProperties,
};

export const textarea_layout = {
  bordered: false,
  style: {
    width: "100%",
    minHeight: 50,
    borderRadius: 25,
    border: "none",
    padding: 15,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontFamily: "Source Sans Pro",
    background: smokeColor,
    boxShadow: cardShadow,
  } as React.CSSProperties,
};

export const borderedInput_layout = {
  bordered: false,
  style: {
    ...input_layout.style,
    background: "#fff",
    border: "1px solid #707070",
  },
};

export const rgbaBackground = `rgba(255,255,255,0.3)`;

export const modal_props: ModalProps = {
  bodyStyle: { textAlign: "center" },
  maskStyle: { background: "#fffffff2" },
  footer: false,
  closable: true,
  closeIcon: React.Fragment,
};
