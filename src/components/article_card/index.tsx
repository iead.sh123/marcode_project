import Icon from '@ant-design/icons';
import { Badge, Card, Image, Space } from 'antd';
import Link from 'next/link';
import React, { FC, useState } from 'react';
import { viewdetails } from 'src/constants';
import { BText } from '../shared';

const ArticleCard: FC<{ article: any }> = ({ article }) => {
	const [hover, setHover] = useState(false);

	return (
		<Badge.Ribbon text={`${article.comments_count} Comments`}>
			<Card
				className='card-container'
				bodyStyle={{ padding: '10px' }}
				style={{
					height: 400,
				}}
				onMouseEnter={() => setHover(true)}
				onMouseLeave={() => setHover(false)}
				cover={
					<div style={{ position: 'relative' }}>
						<Image
							src={article.thumbnails.xsmall_300}
							preview={false}
							className='card-image'
							style={{ objectFit: 'cover', width: '100%', height: 400 }}
							alt='image article'
						/>
						<div
							style={{
								position: 'absolute',
								left: 0,
								top: 0,
								width: '100%',
								height: '100%',
								background: '#333',
								opacity: hover ? 0.5 : 0,
							}}
						>
							<Space
								direction='vertical'
								style={{
									display: 'flex',
									position: 'relative',
									textAlign: 'center',
									top: '40%',
								}}
							>
								<Icon component={viewdetails} />
								<Link href={`Articles/${article.id}`}>
									<a>
										<BText color='primary' lvl={3}>
											View Article
										</BText>
									</a>
								</Link>
							</Space>
						</div>
					</div>
				}
			/>
		</Badge.Ribbon>
	);
};
export default ArticleCard;
