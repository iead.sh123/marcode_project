import Button, { ButtonProps } from 'antd/lib/button';
import React from 'react';
import { darkColor, primaryColor, secondaryColor, successColor, warningColor, dangerColor, thirdColor } from 'src/constants';

export interface BButtonProps extends ButtonProps {
	outline?: boolean;
	color?: 'primary' | 'secondary' | 'white' | 'dark' | 'gray' | 'success' | 'danger' | 'warning';
	background?:
		| 'primary'
		| 'secondary'
		| 'success'
		| 'danger'
		| 'transpart'
		| 'solid'
		| 'gray'
		| 'third'
		| 'secondaryradius'
		| 'primaryradius'
		| 'yellow'
		| 'blue'
		| 'green'
		| 'lightgray'
		| 'black'
		| 'lightred'
		| 'success_radius'
		| 'danger_radius'
		| 'light_brown';
}

export const BButton: React.FC<BButtonProps> = ({ children, outline, color, background, style, ...rest }) => {
	let _color: any;
	switch (color) {
		case 'dark':
			_color = darkColor;
			break;

		case 'gray':
			_color = 'gray';
			break;

		case 'white':
			_color = 'white';
			break;

		case 'primary':
			_color = primaryColor;
			break;

		case 'secondary':
			_color = secondaryColor;
			break;

		case 'success':
			_color = successColor;
			break;

		case 'warning':
			_color = warningColor;
			break;

		case 'danger':
			_color = dangerColor;
			break;
	}

	const _style: React.CSSProperties = {
		// color: _color,
		...style,
		padding: rest.icon ? '0px 10px' : 'none',
		height: 'fit-content',
		width: 300,
		minHeight: 50,
		minWidth: 50,
		// borderRadius: !rest.shape && 0,
		textTransform: 'uppercase',
		boxShadow: '1px 2px 2px #00000026',
	};

	const _styleradius: React.CSSProperties = {
		...style,
		borderRadius: '30px 30px 30px 30px',
		color: 'white',
		padding: 'none',
		height: 'fit-content',
		minHeight: 45,
		minWidth: 50,
		width: 150,
		textTransform: 'capitalize',
		boxShadow: '1px 2px 2px #00000026',
	};

	switch (background) {
		case 'primary':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						..._style,
						border: outline ? `1px solid ${thirdColor}` : 'none',
						background: outline ? 'none' : primaryColor,
						color: outline ? thirdColor : '#fff',
						width: 50,
					}}
				>
					{children}
				</Button>
			);
		case 'secondary':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						..._style,
						border: outline ? `1px solid ${secondaryColor}` : 'none',
						background: outline ? 'none' : secondaryColor,
						color: outline ? secondaryColor : '#fff',
					}}
				>
					{children}
				</Button>
			);
		case 'success':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						..._style,
						border: outline ? `1px solid ${successColor}` : 'none',
						background: outline ? 'none' : successColor,
						color: outline ? successColor : '#fff',
					}}
				>
					{children}
				</Button>
			);
		case 'danger':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						..._style,
						border: outline ? `1px solid ${dangerColor}` : 'none',
						background: outline ? 'none' : dangerColor,
						color: outline ? dangerColor : '#fff',
					}}
				>
					{children}
				</Button>
			);
		case 'gray':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						..._style,
						border: outline ? `1px solid gray` : 'none',
						background: outline ? 'none' : 'gray',
						color: outline ? 'gray' : '#fff',
					}}
				>
					{children}
				</Button>
			);
		case 'transpart':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						backgroundColor: 'rgba(200, 200, 200, 0.25)',
						backdropFilter: 'blur(5px)',
						..._style,
					}}
				>
					{children}
				</Button>
			);
		case 'third':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						backgroundColor: '#ff7b7b',
						..._styleradius,
					}}
				>
					{children}
				</Button>
			);
		case 'secondaryradius':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						..._styleradius,
						background: secondaryColor,
					}}
				>
					{children}
				</Button>
			);
		case 'black':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						..._styleradius,
						background: '#333',
					}}
				>
					{children}
				</Button>
			);
		case 'primaryradius':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						..._styleradius,
						background: primaryColor,
					}}
				>
					{children}
				</Button>
			);
		case 'yellow':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						backgroundColor: '#F4D107',
						..._styleradius,
					}}
				>
					{children}
				</Button>
			);
		case 'blue':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						backgroundColor: '#0E71C6',
						..._styleradius,
					}}
				>
					{children}
				</Button>
			);
		case 'green':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						backgroundColor: '#43A907',
						..._styleradius,
					}}
				>
					{children}
				</Button>
			);
		case 'lightgray':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						backgroundColor: '#AAB2B5',
						..._styleradius,
					}}
				>
					{children}
				</Button>
			);
		case 'lightred':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						backgroundColor: '#FE8F8F',
						..._styleradius,
					}}
				>
					{children}
				</Button>
			);
		case 'success_radius':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						backgroundColor: successColor,
						..._styleradius,
					}}
				>
					{children}
				</Button>
			);
		case 'danger_radius':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						backgroundColor: dangerColor,
						..._styleradius,
					}}
				>
					{children}
				</Button>
			);
		case 'light_brown':
			return (
				<Button
					type='text'
					{...rest}
					style={{
						backgroundColor: '#E0C097',
						..._styleradius,
					}}
				>
					{children}
				</Button>
			);
		default:
			return (
				<Button {...rest} style={_style}>
					{children}
				</Button>
			);
	}
};
