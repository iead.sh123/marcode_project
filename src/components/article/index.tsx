import { Col, Pagination, Row, Space } from 'antd';
import React, { useEffect, useState } from 'react';
import { BText } from '../shared';
import axios from 'axios';
import ArticleCard from '../article_card';

const Articles = () => {
	const [article, setArticle] = useState<any[]>([]);

	const getArticle = (numberPage: number) => {
		axios
			.get(`https://microservice.newsifier.com/api/v2/article/scopes/lat/get/${numberPage}`, {
				headers: {
					'X-Tenant': 'androidworld.newsifier.com',
				},
			})
			.then((response) => {
				const myrepo = response.data.data;
				setArticle(myrepo);
			});
	};

	useEffect(() => {
		getArticle(0);
	}, []);

	const fetchNewArticles = (page: number, pageSize?: number) => {
		getArticle(page);
	};

	return (
		<>
			<div style={{ margin: '4rem' }}>
				<BText color='white' lvl={3} style={{ justifyContent: 'space-between' }}>
					<Space direction='horizontal'>
						<BText color='white' lvl={3}>
							Result For
						</BText>
						<BText color='primary' lvl={3}>
							Articles...
						</BText>
					</Space>
				</BText>
				<Row justify='space-between' style={{ marginTop: '1.5rem' }}>
					{article.map((el) => (
						<Col xl={5} lg={6} md={8} key={el.id} style={{ margin: 4 }}>
							<ArticleCard key={el.id} article={el!} />

							<Space direction='horizontal' style={{ display: 'flex', justifyContent: 'space-between' }}>
								<BText color='primary' lvl={3}>
									Author:
									<BText color='light' lvl={4}>
										{' '}
										{el?.author.username}
									</BText>
								</BText>
								<BText color='primary' lvl={3} style={{ marginLeft: 0 }}>
									Claps:
									<BText color='light' lvl={4}>
										{' '}
										{el?.claps_count}
									</BText>
								</BText>
							</Space>
						</Col>
					))}
				</Row>
				<Row justify='space-between' style={{ marginTop: 20 }}>
					<Col>
						<Pagination onChange={(numberPage) => fetchNewArticles(numberPage - 1)} total={500} />
					</Col>
				</Row>
			</div>
		</>
	);
};
export default Articles;
