import { CaretLeftFilled } from '@ant-design/icons';
import { Row, Col, Space, Image, Divider, Pagination } from 'antd';
import axios from 'axios';
import Link from 'next/link';
import React, { FC, useEffect, useState } from 'react';
import { BButton, BText } from 'src/components/shared';
import { primaryColor } from 'src/constants';
import { useWidth } from 'src/utils';

interface props {
	id: number;
}
const SingleArticle: FC<props> = ({ id }) => {
	const [singleArticle, setsingleArticle] = useState<any>();
	const [commentArticle, setcommentArticle] = useState<any[]>([]);
	const { isMobile } = useWidth();

	const getSingleArticle = () => {
		axios
			.get(`https://androidworld.newsifier.com/api/v1/article-as-visitor/${id}?include=clapsCount,commentsCount`, {
				headers: {
					'X-Tenant': 'androidworld.newsifier.com',
					Authorization: 'Bearer m8tiFyxZrZD1NGWNAjSu7dpPV8hlJOMLOqS2sWCGXXFllxFsHmGwrD3oT2Son1kXaEM6iRL22nLsgBPp',
				},
			})
			.then((response) => {
				console.log('response', response);
				const myrepo = response.data.data;
				console.log('myrepo', myrepo);

				setsingleArticle(myrepo);
			});
	};

	const getCommentArticle = (article_id: number, page: number) => {
		axios
			.get(`https://microservice.newsifier.com/api/v1/article/${article_id}/comments/${page}`, {
				headers: {
					'X-Tenant': 'androidworld.newsifier.com',
				},
			})
			.then((response) => {
				console.log('response', response);
				const myrepo = response.data.data;
				console.log('myrepo', myrepo);

				setcommentArticle(myrepo);
			});
	};

	useEffect(() => {
		getSingleArticle();
		getCommentArticle(id, 0);
	}, []);

	const fetchNewComments = (article_id: number, page: number) => {
		getCommentArticle(article_id, page);
	};
	return (
		<React.Fragment>
			<Header_section />
			<Row justify='space-between' style={{ padding: '0px 62px' }}>
				<Col md={12} xs={24}>
					<Space direction='vertical'>
						<BText color='primary' lvl={2} style={{ padding: '10px 0' }}>
							Single Article
						</BText>
						<Space direction='vertical' style={{ display: 'flex', justifyContent: 'center' }}>
							<Image src={singleArticle?.thumbnails.xsmall_300} width={200} height={200} />
							<Space direction='horizontal'>
								<BText color='primary' lvl={4}>
									Title:{' '}
								</BText>
								<BText color='white' lvl={4}>
									{singleArticle?.title}
								</BText>
							</Space>
						</Space>
					</Space>
				</Col>
				{!isMobile && <Divider type='vertical' style={{ height: 300, alignSelf: 'center', background: primaryColor }} />}
				<Col md={8} xs={24}>
					<BText color='primary' lvl={2} style={{ padding: '10px 0' }}>
						Comment
					</BText>
					{commentArticle.length !== 0 ? (
						commentArticle.map((el) => (
							<Col span={24}>
								<Space direction='vertical' key={el.id}>
									<Space direction='horizontal'>
										<Image src={el?.user_avatar} width={100} height={100} style={{ borderRadius: 50 }} preview={false} />
										<Space direction='vertical'>
											<BText color='primary' lvl={4}>
												{el?.username}
											</BText>
											<BText color='white' lvl={5}>
												{el?.date}
											</BText>
										</Space>
									</Space>
								</Space>
							</Col>
						))
					) : (
						<Col span={24} style={{ margin: 50 }}>
							<BText color='primary' lvl={3}>
								No Comments...!
							</BText>
						</Col>
					)}

					<Row justify='space-between' style={{ marginTop: 20 }}>
						<Col>
							<Pagination onChange={(numberPage) => fetchNewComments(id, numberPage - 1)} total={50} />
						</Col>
					</Row>
				</Col>
			</Row>
		</React.Fragment>
	);
};
export default SingleArticle;

const Header_section: FC = () => {
	return (
		<Row justify='center' style={{ padding: '20px 0' }}>
			<Col span={22}>
				<Space>
					<Link href='/'>
						<a>
							<BButton background='primary' icon={<CaretLeftFilled />} />
						</a>
					</Link>
				</Space>
			</Col>
		</Row>
	);
};
