import React, { FC } from 'react';
import { Row, Col, Divider } from 'antd';
import { BText } from 'src/components/shared';

interface props {}

export const StandardFooter: FC<props> = (props) => {
	return (
		<Row justify='center' gutter={[16, 16]}>
			<Col>
				<BText lvl={3} color='white'>
					All rights reserved © 2021
				</BText>
			</Col>
		</Row>
	);
};
