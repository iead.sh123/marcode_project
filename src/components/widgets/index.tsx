import Head from 'next/head';
import { Layout as AntLayout, Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

import { StandardHeader } from './header';
import { StandardFooter } from './footer';
import { rgbaBackground } from 'src/constants/layout';

const { Header, Footer, Content } = AntLayout;

export const StandardLayout: React.FC<{ children: React.ReactNode }> = ({ children }) => {
	let res = (
		<AntLayout style={{ minHeight: '100vh' }}>
			{/* site header */}
			<Header style={{ padding: 0, height: '100%', background: '#333' }}>
				<StandardHeader />
			</Header>
			{/* site header */}

			<Content style={{ minHeight: '85vh', background: '#333' }}>{children}</Content>

			{/* site footer */}
			<Footer style={{ background: '#333' }}>
				<StandardFooter />
			</Footer>
			{/* site footer */}
		</AntLayout>
	);

	return (
		<Spin
			spinning={false}
			indicator={<LoadingOutlined style={{ fontSize: '2em' }} />}
			style={{
				background: '#333',
				minHeight: '100vh',
				position: 'fixed',
				left: 0,
				top: 0,
			}}
		>
			<Head>
				<title>MarCode</title>
				<meta charSet='utf-8' />
				<meta name='viewport' content='initial-scale=1.0, width=device-width' />
			</Head>

			{res}
		</Spin>
	);
};
