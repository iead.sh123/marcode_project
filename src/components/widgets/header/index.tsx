import React, { FC } from 'react';
import { Row, Col, Space } from 'antd';
import Link from 'next/link';
import { BText } from 'src/components/shared';

import { useRouter } from 'next/router';

const textprops: { color: 'white'; fw: 'bolder' } = {
	color: 'white',
	fw: 'bolder',
};

const nav_layout: React.CSSProperties = {
	position: 'relative',
	height: 110,
};

interface props {}
export const StandardHeader: FC<props> = (props) => {
	const router = useRouter();

	const links = (
		<Space direction='horizontal' size='large' style={{ textTransform: 'uppercase' }}>
			<Link href='/'>
				<a>
					<BText className={`${router.pathname === '/' || router.pathname === '/Articles/[id]' ? 'active' : ''}`} {...textprops}>
						Articles
					</BText>
				</a>
			</Link>
		</Space>
	);

	return (
		<>
			<Row justify='center' align='middle' style={nav_layout}>
				<Col span={22}>
					<Row justify='space-between' gutter={[16, 0]}>
						<Col>{links}</Col>
					</Row>
				</Col>
			</Row>
		</>
	);
};
