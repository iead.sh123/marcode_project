import React, { FC } from "react";
import Home from "src/components/article";

interface props {}

const index: FC<props> = () => {
  return <Home />;
};

export default index;

// export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
//   return { redirect: { destination: "/", statusCode: 307 } };
// };
