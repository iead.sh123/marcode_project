import { useRouter } from 'next/router';
import React, { FC } from 'react';
import SingleArticle from 'src/components/article/single';

interface props {}
const index: FC<props> = (props) => {
	const {
		query: { id },
	} = useRouter();

	return <SingleArticle id={Number(id)} />;
};
export default index;
