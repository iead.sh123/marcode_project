import React, { FC } from 'react';
import Articles from 'src/components/article';

interface props {}

const index: FC<props> = () => {
	return <Articles />;
};

export default index;
