import Head from 'next/head';
import React from 'react';
import { ConfigProvider, notification } from 'antd';
import '../shared/style.less';
import '../shared/antd.less';

import { StandardLayout } from '../components/widgets';

export default function App({ Component, pageProps }: any) {
	return (
		<>
			<Head>{/* <link rel='shortcut icon' href='/assets/.ico' /> */}</Head>

			<ConfigProvider>
				<StandardLayout>
					<Component {...pageProps} />
				</StandardLayout>
			</ConfigProvider>
		</>
	);
}
